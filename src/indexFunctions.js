//Nav Bar Functions

class History { //GRADING: MANAGE
    constructor(){
        this.UndoRedos =[];
        this.index = 0;
    }

    //new UndoRedo, remove everything after the current UndoRedo index
    //and append the new function
    executeAction(cmd){
        this.UndoRedos.length = this.index; //trims length from 0 to index
        this.UndoRedos.push(cmd);
        this.index = this.UndoRedos.length

        //run the UndoRedo and update
        cmd.exec();
        updateUI();
    }


    //undo called. Call the undo functin on the current UndoRedo and move back one
    undoCmd(){
        if(this.index > 0)
        {
            let cmd = this.UndoRedos[this.index-1];
            cmd.undo();
            this.index= this.index - 1;
            updateUI();
        }
    }

    //redo called. Call the execution function on the current UndoRedo and move forward one
    redoCmd(){
        if(this.index < this.UndoRedos.length)
        {
            let cmd = this.UndoRedos[this.index];
            cmd.exec();
            this.index = this.index + 1;
            updateUI();
        }
    }


    //is undo available
    canUndo(){
        return this.index !== 0;
    }

    //is redo available
    canRedo(){
        return this.index < this.UndoRedos.length;
    }
}

class change_button{ //GRADING: COMMAND
    constructor(button) {
        this.button = button;
    }

    exec(){
        switch(this.button.value){
            case '0':{
                this.button.className = "pole";
                this.button.innerHTML = 'pole';
                this.button.value = '1';
                break;
            }
            case '1':{
                this.button.className = 'house';
                this.button.innerHTML = 'house';
                this.button.value = '2';
                break;
            }
            case '2':{
                this.button.className = 'empty';
                this.button.innerHTML = 'empty';
                this.button.value = '0';
                break;
            }
            default:{
                this.button.className = 'empty';
                this.button.innerHTML = 'empty';
                this.button.value = '0';
                break;
            }
        }
    }

    undo(){
        switch(this.button.value){
            case '0':{
                this.button.className = 'house';
                this.button.innerHTML = 'house';
                this.button.value = '2';
                break;
            }
            case '1':{
                this.button.className = 'empty';
                this.button.innerHTML = 'empty';
                this.button.value = '0';
                break;
            }
            case '2':{
                this.button.className = 'pole';
                this.button.innerHTML = 'pole';
                this.button.value = '1';
                break;
            }
            default:{
                this.button.className = 'empty';
                this.button.innerHTML = 'empty';
                this.button.value = '0';
                break;
            }
        }
    }
}

class set_preconfig{ //GRADING: COMMAND
    constructor(pattern){
        this.new = pattern;
        this.past = get_pattern();
    }

    exec(){
        make_grid(this.new);
    }

    undo(){
        make_grid(this.past);
    }
}


function get_pattern(){
    let pattern = [];
    let grid = document.getElementById('grid_to_modify');
    for( let i = 0; i < grid.children.length; i++){
        pattern.push(grid.children[i].value);
    }
    return pattern;
}

function set_grid(){
    let pattern = ['0', '0', '0'];
    if(window.location.href.toString().includes("?")) {
        pattern = window.location.href.split("?").pop().split(',');
    }
    console.log("pattern: " + pattern);
    make_grid(pattern);

}


const hist = new History();

function updateUI(){
    document.getElementById("undo").disabled = !hist.canUndo();
    document.getElementById("redo").disabled = !hist.canRedo();
}

function goHome(){
    window.location.href="index.html";
}

//button functions
function undo(){
    hist.undoCmd();
}

function redo(){
    hist.redoCmd();
}

function make_grid(pattern){
    let grid = document.getElementById('grid_to_modify');
    while (grid.firstChild) {
        grid.removeChild(grid.firstChild);
    }

    for(let i = 0; i < pattern.length; i++){
        let newBtn = document.createElement('BUTTON');
        newBtn.value = pattern[i];
        if(newBtn.value == '0'){
            newBtn.className = "empty";
            newBtn.innerHTML = "empty";
        }
        else if(newBtn.value == '1'){
            newBtn.className = "pole";
            newBtn.innerHTML = "pole";
        }
        else if(newBtn.value == '2'){
            newBtn.className = "house";
            newBtn.innerHTML = "house";
        }
        newBtn.onclick = function () {change_grid_space(newBtn);};
        grid.appendChild(newBtn)
    }
}

function new_row(){
    let pattern = get_pattern();
    for(let i = 0; i <3; i++){
        pattern.push('0');
    }
    //GRADING: ACTION
    hist.executeAction(new set_preconfig(pattern));
}

function delete_row(){
    let pattern = get_pattern();
    for(let i = 0; i < 3; i++){
        pattern.pop();
    }
    hist.executeAction(new set_preconfig(pattern));
}

function save_btn(){
    window.location.href = "write?write=1&stuff=" + get_pattern()
}

function reset_btn(){
    let pattern = ['0', '0', '0'];
    //GRADING: ACTION
    hist.executeAction(new set_preconfig(pattern));
}

function set_poles(){
    let pattern = ['1', '1', '1', '1', '1', '1'];
    //GRADING: ACTION
    hist.executeAction(new set_preconfig(pattern));
}

function set_mixed(){
    let pattern = ['0', '0', '1', '2', '0', '2', '1', '2', '1'];
    //GRADING: ACTION
    hist.executeAction(new set_preconfig(pattern));
}

//grid functions
function change_grid_space(button){
    //GRADING: ACTION
    hist.executeAction(new change_button(button));
}

function loadFile(fileName){
    window.location.href = "tmp/" + fileName;
}

function downloadFile(fileName){
    window.location.href = "download/" + fileName;
}

function deleteFile(fileName){
    window.location.href = "delete/" + fileName;
}


