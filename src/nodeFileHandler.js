const Mustache = require('mustache');
const fs = require("fs");
let formidable = require('formidable');
const path = require("path");
const express = require("express");

let folder = "./tmp"
let save_file = folder + '/save_file.txt';
function pullData(data){

    //if no files yet, stop processing
    if(!fs.existsSync(folder))
        return;

    //template style
    data['filelist'] = fs.readdirSync(folder);

    //raw html style
    /*
    var items = fs.readdirSync(folder);
    var list = "<ul>";
    for( item of items){
        list += "<li>" + item + "</li>";
    }
    list += "</ul>";
    data['filelist'] = list;
    */
}


function getParams(req){
    let host = "http://" + req.headers.host;
    const query = new URL(req.url, host);

    return Object.fromEntries(query.searchParams);
}
function load(req, res, params){
    var data = getParams(req);

    pullData(data);

    data= Object.assign(data, params);

    if(req.url === "/favicon.ico"){
        req.url = "/index.html";
    }

    try{
        let url =  path.join(__dirname,req.url);
        let content = fs.readFileSync(url);
        let output = Mustache.render(content.toString(), data);

        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(output);

    }catch(err){
        //data dump
        console.log(err);
    }
    res.end();
}


//move mapping out of server call to app.get
function init(app, urlRoot = "/") {

    //override default location
    folder = path.join(__dirname, folder);
    save_file = folder + '/save_file.txt';

    app.get( urlRoot, function(req, res) {
        res.redirect(urlRoot + "./index.html?0,0,0");
    });


    app.get( urlRoot + "FileManager.html", function(req, res) {
        load(req,res);
    });

    app.get(  urlRoot + "download/:file", function(req, res) {
        let fileName = folder + "/" + req.params.file;
        res.download(fileName);
    });


    app.get(  urlRoot + "write", function(req, res) {
        let host = "http://" + req.headers.host;
        const query = new URL(req.url, host);
        var data = Object.fromEntries(query.searchParams);
        var content = "";
        content += data["stuff"];

        try{
            if(!fs.existsSync(folder)){
                fs.mkdirSync(folder);
            }
        }catch(err){
            console.error(err)
        }


        fs.writeFile(save_file, content, function (err) {
            if (err) throw err;
        });

        res.redirect(urlRoot + "./index.html?" + content);
        res.end();
    });

    // app.get(urlRoot + "index.html", function(req, res){
    //     let content = "0,0,0";
    //     try{
    //         if(fs.existsSync(save_file)){
    //             content = fs.readFileSync(save_file);
    //             console.log("content: " + content);
    //         }
    //     }catch(err) {
    //         console.error(err);
    //     }
    //
    //     req.url = req.url.split("?")[0];
    //     console.log("url: " + req.url);
    //
    //     req.query = content;
    //     console.log("query: " + req.query.toString());
    //
    //     load(req, res);
    // });

    app.post(  urlRoot + "upload", function(req, res) {
        var form = new formidable.IncomingForm();

        //these next three lines are optional, but a good idea
        form.multiples = false;
        form.maxFileSize = 50 * 1024 * 1024; // 5MB
        form.uploadDir = folder;

        try{
            if(!fs.existsSync(folder)){
                fs.mkdirSync(folder);
            }
        }catch(err){
            console.error(err)
        }

        form.parse(req, function (err, fields, files) {
            if (err != null) {
                console.log(err)
            }
            else{
                var oldpath = files.fileToUpload[0].filepath;
                var newpath = folder + "/" + files.fileToUpload[0].originalFilename;
                fs.rename(oldpath, newpath, function (err) {
                    if(err){
                        console.log(err);
                    }
                    //no error? all good, and all done!
                });
            }
            res.redirect(urlRoot + "./FileManager.html");
        });

    });

    app.get(  urlRoot + "tmp/:file", function(req, res) {
        let fileName = req.params.file;
        fileName = folder + "/" + fileName;
        let content = "";
        try{
            content = fs.readFileSync(fileName);
        }catch(err){
            console.error(err);
        }

        res.redirect(urlRoot + "./index.html?" + content);
        res.end();

    });

    app.get(urlRoot + "delete/:file", function(req, res){
       let fileName = folder + '/' + req.params.file;
       try{
           fs.rmSync(fileName);
       }catch(err){
           console.error(err);
       }

       res.redirect(urlRoot + "FileManager.html");
    });

}


function runPage(req, res) {
    load(req,res);
}

module.exports = {init, runPage};
