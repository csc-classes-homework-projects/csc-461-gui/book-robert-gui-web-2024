const http = require('http');
const fs = require('fs');
const express = require('express');
const path = require('path');

var app = express();

let fileTransfer = require('./nodeFileHandler');

fileTransfer.init(app);

app.use(express.static(path.join(__dirname)));
app.get("/|index.html", fileTransfer.runPage);
app.listen(8080);
